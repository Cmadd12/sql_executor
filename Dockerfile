FROM python:3
ADD execute_sql.py /
ADD requirements.txt /
RUN pip install -r requirements.txt
