import sys
import re
import json 
import boto3
import psycopg2
from psycopg2 import sql

dynamodb_client = boto3.client('dynamodb', region_name='us-east-1')

#pass in arguement from batch 1: sql name, 2: date parameter
sql2run_s3_foldername =     sys.argv[1]
sql2run_template_filename = sys.argv[2]
sql2run_params =            sys.argv[3]
sql2run_ident =             sys.argv[4]
step_fn_taskid =            sys.argv[5]
step_fn_execution_id =      sys.argv[6]
step_fn_execution_start =   sys.argv[7]

print("User specified SQL folder " + str(sql2run_s3_foldername) + ".")
print("User specified SQL template " + str(sql2run_template_filename) + ".")
print("User specified SQL identifiers " + str(sql2run_ident) + ".")
print("User specified SQL parameters " + str(sql2run_params) + ".")
print("Step function task id paramter " + str(step_fn_taskid) + ".")
print("Step function execution id paramter " + str(step_fn_execution_id) + ".")
print("Step function execution start time " + str(step_fn_execution_start) + ".")

#unpack object into python dictionary
params_dict = json.loads(sql2run_params)
identf_dict = json.loads(sql2run_ident)

#get prep_q from s3
s3 = boto3.resource('s3')
obj = s3.Object('my_bucket', 'my_folder/' + sql2run_s3_foldername + sql2run_template_filename)
sql_template = obj.get()['Body'].read().decode('utf-8') 
print('Reading SQL from the following location ' + str(obj) + ".")

#connect to database and run query
conn=psycopg2.connect(
            dbname= 'dev', 
            host='aws-host', 
            port= '5439', 
            user= 'test', 
            password= 'testtest')
cur = conn.cursor()
print ("Connected to RDBMS.")

#trfm for sql tbl time and folder parameters
today = str(step_fn_execution_start[0:19]) #drop AM or PM?
today_trfm = today.replace('-', '_').replace(':', '_')
sql2run_s3_foldername_trfm = sql2run_s3_foldername.replace('-', '_')

# #concat s3 folder name and execution date on sql passed param
# for k,v in identf_dict.items():   
#     if "_tbl" in k:
#         identf_dict[k] = str(sql2run_s3_foldername_trfm + '_' + v + '_' + today_trfm)

#adjust identifiers structure to allow for cur.execute
identf_dict_psycop = [{k: sql.Identifier(v)} for k, v in identf_dict.items()]
merged_dict = {}
for item in identf_dict_psycop:
    merged_dict.update(item)

#if v of k is a list make into a tuple; else pass along!
for k,v in params_dict.items():
    if isinstance(v, list):
        params_dict[k] = tuple(v)

#print out SQL passing into curr execute
mog_output = str(cur.mogrify(sql.SQL(sql_template).format(**merged_dict).as_string(conn), params_dict))
print('Executing the following query: ' + mog_output)
print('Query executing.')

#execute query
cur.execute(sql.SQL(sql_template).format(**merged_dict).as_string(conn), params_dict)
conn.commit()

print("Query results: ")
sql_status = cur.statusmessage
print(sql_status)

record_count = -1

try:
    sql_result_records = cur.fetchall()
    for x in sql_result_records:
        print(x)
    record_count = len(sql_result_records)
    print("Record count was " + str(record_count) + ".")
except:
    print("No records")
conn.close()


dyno_record = {"view_execution_id": step_fn_execution_id,
               "view_task_id": step_fn_taskid,
               "data_parameters": 
                    {"sql2run_ident": sql2run_ident,
                     "sql2run_params": sql2run_params, 
                     "sql2run_template": sql2run_template_filename,
                     "sql2run_folder": sql2run_s3_foldername}, 
                "data_results":
                     {"sql_status": sql_status,
                      "result_records": record_count}}

print("Writing new record to dynamodb: ")
print(dyno_record)

dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
table = dynamodb.Table('tablename')
table.put_item(Item=dyno_record)
